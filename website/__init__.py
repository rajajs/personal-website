
## Imports

# flask and extensions
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_frozen import Freezer
# from flask_admin import Admin

## init app
app = Flask(__name__)
app.config.from_object('config')

db = SQLAlchemy(app)
freezer = Freezer(app)
# admin = Admin(app)

# import views at end
from  website import views


if __name__ == '__main__':
    app.run()
    # freezer.freeze()
