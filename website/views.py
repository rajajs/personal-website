

from flask import render_template, redirect, url_for

from website import app, db
from website.models import Photo , Publication, Blog, Talk, Trek, Code
# from utils import 

from markdown import markdown


@app.route('/')
def home():
    return render_template('pages/home.html',
                           current_page = 'home')


## 404 - page not found
@app.errorhandler(404)
def page_not_found(error):
    return render_template('pages/page_not_found.html'), 404


@app.route('/talks/')
def talks():
    talks = Talk.query.\
            order_by(Talk.talk_date.desc()).\
            all()
    return render_template('pages/talks.html',
                           talks=talks,
                           current_page = 'talks')

@app.route('/talk/<slug>/<int:notes>/')
def talk(slug, notes):
    """
    if notes is 1, use slides with notes
    """
    talk = Talk.query.\
           filter(Talk.slug == slug).\
           first()

    if notes == 0:
        filename = talk.slide_filename
    elif notes == 1:
        filename = talk.slide_notes_filename
    elif notes == 2:
        filename = talk.pdf_filename
    
    return render_template('pages/talk_slides.html',
                           talk=talk, notes=notes,
                           filename = filename,
                           current_page='talks')


@app.route('/code/')
def code():
    projects = Code.query.all()

    return render_template('pages/code.html',
                           projects=projects,
                           current_page='code')


@app.route('/publications/<format>/')
def publications(format):
    """
    format can be 'rich' or 'plain'
    """
    pubs = Publication.query.\
           order_by(Publication.year.desc()).\
           all()

    summary = {}
    summary['total_cit_count'] = sum([pub.cit_count
                                      for pub in pubs
                                      if pub.cit_count])

    # article type
    pubtypes = [pub.pubtype for pub in pubs]
    pub_type_counts = {
        'original_research': 0,
        'letter': 0,
        'editorial': 0,
        'case_report': 0,
        'book': 0,
        'review':0
    }
    for ptype in pubtypes:
        try:
            pub_type_counts[ptype] += 1
        except KeyError:
            pass
    summary['pub_type_counts'] = pub_type_counts
    
    # publication year
    pubyears = [pub.year for pub in pubs]
    pub_year_counts = {}
    for pyear in pubyears:
        if pyear in pub_year_counts:
            pub_year_counts[pyear] += 1
        else:
            pub_year_counts[pyear] = 1

    summary['pub_year_counts'] = pub_year_counts
    summary['pub_years'] = sorted(pub_year_counts.keys())

    if format == 'rich': 
        return render_template('pages/publications.html',
                               pubs = pubs, summary=summary,
                               current_page='publications')
    elif format == 'plain':
        return render_template('pages/publications2.html',
                               pubs = pubs, summary=summary,
                               current_page='publications')        


@app.route('/publications_text')
def publications_text():
    pubs = Publication.query.\
           order_by(Publication.year.desc()).\
           all()

    return render_template('pages/publications_text.html', pubs=pubs)
    


# @app.route('/photography/')
# def photography():
#     photos = Photo.query.\
#              order_by(Photo.date_taken.desc()).\
#              all()
#     return render_template('pages/photography.html',
#                            photos = photos,
#                            current_page='photography')


@app.route('/photography/')
def photography():
    return render_template('pages/photography2.html')


@app.route('/resume/')
def resume():
    return render_template('pages/resume.html',
                           current_page='resume')


@app.route('/blog/')
def blog():
    blogs = Blog.query.\
            order_by(Blog.date_created.desc()).\
            all()
    
    tags = sum([b.tags.split(',') for b in blogs], [])  # makes a flat list of all tags

    # remove spaces around
    tags = [t.strip() for t in tags]
    
    tag_counts = {}
    for i in range(len(tags)):
        if not tag_counts.has_key(tags[i]):
            tag_counts[tags[i]] = 1
        else:
            tag_counts[tags[i]] += 1
            
    # convert to list of tuples
    tags = [(k, tag_counts[k]) for k in tag_counts]
    tags = sorted(tags, key=lambda x: x[1], reverse=True)

    return render_template('pages/blog.html',
                           blogs=blogs, tags=tags,
                           current_page='blog')


@app.route('/blog/<slug>/')
def blog_post(slug):
    #TODO: change to get or 404
    blog = Blog.query.\
           filter(Blog.slug == slug).first()
    
    return render_template('pages/blog_post.html',
                           current_page='blog',
                           blog=blog)


@app.route('/treks/')
def treks():
    treks = Trek.query.\
            order_by(Trek.date.desc()).\
            all()
    return render_template('pages/treks.html',
                           current_page='treks',
                           treks=treks)
