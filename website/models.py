from website import db
from markdown import markdown

class Publication(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    author = db.Column(db.String(512))
    title = db.Column(db.String(256))
    journal = db.Column(db.String(64))
    year = db.Column(db.Integer())
    volume = db.Column(db.String(8))
    iss = db.Column(db.String(8))
    pages = db.Column(db.String(16))
    # first_pg = db.Column(db.String(8))
    # last_pg = db.Column(db.String(8))
    
    pubdate = db.Column(db.String(32))
    pubtype = db.Column(db.String(32))
    status = db.Column(db.String(32))
    doi = db.Column(db.String(64))

    abstract = db.Column(db.String(4096))

    # non bibtex fields
    impact_factor = db.Column(db.Float())
    pmid = db.Column(db.Integer())
    pdf = db.Column(db.String(64)) # path to full text pdf
    cit_count = db.Column(db.Integer) # number of citations
    cit_count_date = db.Column(db.Date()) 


class Photo(db.Model):
    """
    """
    id = db.Column(db.Integer, primary_key=True)

    filename = db.Column(db.String(32))

    # photo info
    title = db.Column(db.String(64))
    caption = db.Column(db.String(512))
    location = db.Column(db.String(128))

    # exif info
    date_taken = db.Column(db.Date())
    focal_length = db.Column(db.Float())
    aperture = db.Column(db.Float())
    shutter = db.Column(db.String(16)) # in seconds
    iso = db.Column(db.String(8))
    
    # hardware
    camera_manufacturer = db.Column(db.String(32))
    camera_model = db.Column(db.String(32))
    lens = db.Column(db.String(64))
    

class Talk(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    slide_filename = db.Column(db.String(128)) # relative
    slide_notes_filename = db.Column(db.String(128)) # slides with notes
    pdf_filename = db.Column(db.String(128)) # pdf for download
    video_url = db.Column(db.String(128))
    
    image = db.Column(db.String(32)) # image for the talk - 400x300 px

    title = db.Column(db.String(512))
    slug = db.Column(db.String(128))
    talk_date = db.Column(db.Date())
    meeting = db.Column(db.String(128))
    location = db.Column(db.String(128))
    
    category = db.Column(db.String(32)) # select field
    tags = db.Column(db.String(64)) # comma separated tags


class Blog(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    md_content = db.Column(db.Text()) # content in markdown
    title = db.Column(db.String(1024))
    slug = db.Column(db.String(1024))
    tags = db.Column(db.String(512))  # comma separated tags as string
    date_created = db.Column(db.Date())
    md_summary = db.Column(db.Text()) # for blog listing page

    
    def html_content(self):
        return markdown(self.md_content)

    def html_summary(self):
        return markdown(self.md_summary)


class Trek(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128))
    date = db.Column(db.Date())
    difficulty = db.Column(db.String(32))  # Easy / Moderate / Difficult
    image = db.Column(db.String(128))  # relative path to image file
    location = db.Column(db.String(32))    # latitude, longitude
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)    
    duration = db.Column(db.Integer())  # days
    description = db.Column(db.Text())


class Code(db.Model):
    """
    Coding projects
    """
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(128))
    description = db.Column(db.Text())
    image = db.Column(db.String(128))  # relative path to image filee in /static/img/code/

    more_info_link = db.Column(db.String(512))
    downloads_link = db.Column(db.String(512))
    source_link = db.Column(db.String(512))
