from flask_script import Manager, Server
from flask_migrate import Migrate, MigrateCommand

from website import app, db
from website.models import Photo, Publication, Talk, Blog, Trek, Code 

import os
from datetime import datetime
from PIL import Image, ExifTags
import bibtexparser

#########################################
# admin added here. this removes it from frozen flask
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView


admin = Admin(app)

class BlogView(ModelView):
    form_widget_args = {
        'md_content': {
            'rows': 10,
            'cols': '1'
        }
    }

class PublicationView(ModelView):
   column_exclude_list = ['abstract']
   column_searchable_list = ['title']
   column_editable_list = ['pubtype', 'status', 'impact_factor', 'pmid', 'cit_count', 'cit_count_date']


class TalkView(ModelView):
   column_editable_list = ['category']
    
admin.add_view(ModelView(Photo, db.session))
admin.add_view(PublicationView(Publication, db.session))
admin.add_view(TalkView(Talk, db.session))
admin.add_view(BlogView(Blog, db.session))
admin.add_view(ModelView(Trek, db.session))
admin.add_view(ModelView(Code, db.session))

###################################################    



manager = Manager(app)
migrate = Migrate(app, db)

# runserver
port = 8090 # or get from env if required
manager.add_command('runserver', Server(host='0.0.0.0', port=port))
manager.add_command('db', MigrateCommand)


@manager.command
def bib2pub(bibfile):
   """
   From a local bibtex file,
   add publications.
   identify dups by doi, pmid
   consider doing by title, but its not really a unique field, so not for now
   """
   needed_keys = ["author","title","journal","year","volume",
                  "iss","pages","pubdate","pubtype","status",
                  "doi","abstract"," pmid"]

   with open(bibfile) as bf:
      bib_db = bibtexparser.load(bf)
      
   for pub in bib_db.entries:
      # dup check with doi
      if 'doi' in pub.keys():
         match = Publication.query.\
              filter(Publication.doi == pub['doi']).first()
         if match:
            print 'skipping because of existing doi: ', pub['doi']
            continue

      # dup check with pmid
      if 'pmid' in pub.keys():
         match = Publication.query.\
                 filter(Publication.pmid == pub['pmid']).first()
         if match:
            print 'skipping because of pmid: ', pub['pmid']
            continue


      # remove keys not in model
      pop_list = [k for k in pub.keys() if k not in needed_keys]
      for k in pop_list:
         pub.pop(k)

      # remove braces in title
      if pub["title"].startswith('{'):
         pub["title"] = pub["title"][1:-1]
         
      # add to db
      publication  = Publication(**pub)
      db.session.add(publication)
      db.session.commit()
      print 'Added publication: ', pub['title']
   


def get_exif(img):
    """
    Given image file opened with PIL
    return dict of exif values
    non existent values are marked as empty string (None for date)
    """
    exif = {
        'Orientation': '',
        'date_taken': '',
        'focal_length': 0,
        'aperture': 0,
        'shutter': '',
        'manufacturer': '',
        'model': '',
        'iso': 0
    }

    # if img has not exif info
    if not img._getexif():
        return exif

    exif = {
            ExifTags.TAGS[k]: v
            for k, v in img._getexif().items()
            if k in ExifTags.TAGS
        }

    exif['date_taken'] = datetime.strptime(exif['DateTimeOriginal'], '%Y:%m:%d %H:%M:%S')
      
    focal_length = exif['FocalLength']
    exif['focal_length'] = focal_length[0] / focal_length[1]
    
    aperture = exif['ApertureValue']
    if aperture[1] != 0:
        exif['aperture'] = aperture[0] / float(aperture[1])
    else:
         exif['aperture'] = 0
         
    shutter_1, shutter_2 = exif['ExposureTime']
    if shutter_1 < shutter_2: # less than one sec
        exif['shutter'] = "1/" + str(shutter_2 / shutter_1)
    else:
        exif['shutter'] = str(shutter_1 / shutter_2)
             
    exif['manufacturer'] = exif.get('Make', '')

    exif['model'] = exif.get('Model', '')
    if exif['model'].startswith('Canon'):
        exif['model'] = exif['model'][5:]  # remove canon

    exif['iso'] = exif.get('ISOSpeedRatings', '')
      
    return exif    
      

@manager.command
def process_photos_nanogallery(folderpath):
    """
    Switched to nanogallery for photos in May 2019
    Photos are provided by a script - nanogalleryprovider which prepares thumbnails 
    
    Process photos in a folder before uploading

    We need to resize photos if needed, rotate if needed, 
    add txt file which will carry the title and description.

    # auto process title and descr
    Add date to title 
    Add exif info to description

    Do not alter txt file if already present
    """
    # read exif orientation tag and rotate accordingly
    rotations = {
        3: Image.ROTATE_180,
        6: Image.ROTATE_270,
        8: Image.ROTATE_90
    }   
    
    # geom for photos
    photo_size = (1400, 1400)   # max width / height for photos    

    # list photos in this folder - full path
    image_extensions = ['jpg', 'jpeg', 'JPG']
    photo_files = [f for f in os.listdir(folderpath)
                 if any(f.endswith(ext) for ext in image_extensions)] 
    photo_files = [os.path.join(folderpath, filepath) for filepath in photo_files]

    # filter files which dont have the txt file
    unprocessed_files = [f for f in photo_files
                         if not os.path.isfile(
                                 os.path.splitext(f)[0] + '.txt')]

    #
    for f in unprocessed_files:
        print '\n--------------------'
        print 'starting', f
        
        # open with PIL
        try:
            img = Image.open(f)
        except IOError:
            continue
      
        # get exif info
        exif = get_exif(img)

        # rotate image
        orientation = exif['Orientation']
        if orientation in rotations:
            img = img.transpose(rotations[orientation])
            print 'rotated'

        # resize
        img.thumbnail(photo_size, Image.ANTIALIAS)
        if 'exif' in img.info:
            img.save(f, quality=85, exif=img.info['exif']) # try to retain exif
        else:
            img.save(f, quality=85)
        
        # title - title | date (month year) | place
        try:
            datestring = exif['date_taken'].strftime('%b_%Y')
        except:
            datestring = ''
        
        title = "_|_".join([
            "Untitled",
            datestring,
            "Place"
        ])
        
        # description - exif
        description = "_|_".join([
            exif['manufacturer'] + "_" + exif['model'],
            "f" + "%0.1f" % exif['aperture'],
            "%d" % exif['focal_length'] + "mm",
            exif['shutter'] +'sec',
            "ISO" + "%d" % exif['iso']
        ])

        with open(os.path.splitext(f)[0] + '.txt', 'w') as fi:
            fi.write('title= %s\n' % title)
            fi.write('description= %s' % description)
            fi.close()
            print 'written txt file'
        
        
            
@manager.command
def process_photos():
   """
   Identify new pics
   resize them if needed
   create thumbails
   populate db table with filename, exif info
   """
   # all photos are in this folder
   photo_folder = os.path.abspath('website/static/img/photos')
   thumbs_folder = os.path.abspath('website/static/img/thumbs')

   # read exif orientation tag and rotate accordingly
   rotations = {
      3: Image.ROTATE_180,
      6: Image.ROTATE_270,
      8: Image.ROTATE_90
   }   
   
   # geom for photos
   photo_size = (1200, 1200)   # max width / height for photos
   
   # geom for thumbnails
   thumb_size = (400, 400)
   
   # list photos in this folder
   filenames = [f for f in os.listdir(photo_folder) if os.path.isfile(os.path.join(photo_folder,f))]

   for f in filenames:
      print '\n----------------'
      print 'starting', f
      
      # is it already in db ?
      p = Photo.query.\
          filter(Photo.filename == f).\
          first()
      if p:
         print 'already in db'
         continue

      # open with PIL
      try:
         img = Image.open(os.path.join(photo_folder,f))
      except IOError:
         continue
      
      # get exif info
      exif = {
         ExifTags.TAGS[k]: v
         for k, v in img._getexif().items()
         if k in ExifTags.TAGS
      }      

      # rotate image
      orientation = exif['Orientation']
      if orientation in rotations:
         img = img.transpose(rotations[orientation])
         print 'rotated'

      # resize
      img.thumbnail(photo_size, Image.ANTIALIAS)
      img.save(os.path.join(photo_folder, f), quality=85)
      
      # create thumbnail
      img.thumbnail(thumb_size, Image.ANTIALIAS)
      img.save(os.path.join(thumbs_folder, f), quality=95)

      # process exif
      date_taken = datetime.strptime(exif['DateTimeOriginal'], '%Y:%m:%d %H:%M:%S')
      print 'date taken', date_taken
      
      focal_length = exif['FocalLength']
      focal_length = focal_length[0] / focal_length[1]
      print 'focal length', focal_length

      aperture = exif['ApertureValue']
      if aperture[1] != 0:
         aperture = aperture[0] / float(aperture[1])
      else:
         aperture = 0
      print 'aperture', aperture

      shutter_1, shutter_2 = exif['ExposureTime']
      if shutter_1 < shutter_2: # less than one sec
         shutter = "1/" + str(shutter_2 / shutter_1)
      else:
         shutter = str(shutter_1 / shutter_2)
      print 'shutter', shutter

      camera_manufacturer = exif.get('Make', '')

      camera_model = exif.get('Model', '')
      if camera_model.startswith('Canon'):
         camera_model = camera_model[5:]  # remove canon

      lens = exif.get('LensModel', '')

      iso = exif.get('ISOSpeedRatings', '')
      
      # add to db - filename, exif
      p = Photo()
      p.filename = f  # only the final name

      p.title = ''
      p.caption = ''
      p.location = ''
      
      p.date_taken = date_taken 
      p.focal_length = focal_length
      p.aperture = aperture
      p.shutter = shutter
      p.iso = iso
      
      p.camera_manufacturer = camera_manufacturer 
      p.camera_model = camera_model
      p.lens = lens

      db.session.add(p)
      db.session.commit()
      print 'added to db'
      

# for production use init to launch app
if __name__ == "__main__":
   manager.run()
